from __future__ import print_function
from flask import Flask, request, render_template, send_from_directory, redirect
from flask_api.decorators import set_parsers
from flask_api.parsers import MultiPartParser
import requests
import os
import json
import sys

app = Flask(__name__)
app.config['DEFAULT_PARSERS'] = [
    'flask.ext.api.parsers.JSONParser',
    'flask.ext.api.parsers.URLEncodedParser',
    'flask.ext.api.parsers.MultiPartParser'
]


def send_url_to_server2(url):
    try:
        response = requests.post('http://localhost:8080/download/', json={"url": url})
    except requests.exceptions.ConnectionError:
        return {'status': 400}
    return json.loads(response.content)



@app.route('/status', methods=['GET'])
def status():
    return render_template('status.html', file_id=request.args.get('id'))


@app.route("/", methods=['GET', 'POST'])
@set_parsers(MultiPartParser)
def upload():
    if request.method == 'GET':
        return render_template('download.html')
    elif request.method == 'POST':
        file_url = request.form.get('url')
        result = send_url_to_server2(file_url)
        unique_id = result["data"]["UniqID"]
        if result['status'] == 200:
            return redirect(f"/status?id={unique_id}", code=301)
        else:
            return render_template('download.html')


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 30000))
    app.run(debug=True, host='127.0.0.1', port=port)
