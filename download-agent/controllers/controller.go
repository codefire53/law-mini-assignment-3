package controllers

import (
	"encoding/json"
	"download-agent/services"
	"net/http"
	"google.golang.org/grpc"
)
// APIResponse ...
type APIResponse struct {
	Status int         `json:"status"`
	Data   interface{} `json:"data"`
}

// APIResponseError ...
type APIResponseError struct {
	Error  string `json:"error"`
	Status int    `json:"status"`
}

// Response handler
func Response(w http.ResponseWriter, httpStatus int, data interface{}) {
	apiResponse := new(APIResponse)
	apiResponse.Status = httpStatus
	apiResponse.Data = data

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	json.NewEncoder(w).Encode(apiResponse)
}

// ResponseError handler
func ResponseError(w http.ResponseWriter, httpStatus int, err error) {
	apiResponse := new(APIResponseError)
	apiResponse.Error = err.Error()
	apiResponse.Status = httpStatus

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	json.NewEncoder(w).Encode(apiResponse)
}

// DownloadRequest ...
type DownloadRequest struct {
	URL string
}

// DownloadResponse ...
type DownloadResponse struct {
	URL string
	UniqID string
}
//DownloadController ...
type DownloadController struct {
}

func (d *DownloadController) Download(res http.ResponseWriter, req *http.Request) {
	conn, err := grpc.Dial("localhost:7001", grpc.WithInsecure())
	if err != nil {
		ResponseError(res, http.StatusBadRequest, err)
		return
	}
	downloadClient := services.NewDownloadServiceClient(conn)
	reqBody, err := d.decodeRequest(req)
	if err != nil {
		ResponseError(res, http.StatusBadRequest, err)
		return
	}
	reqInput := &services.DownloadRequest{Url: reqBody.URL}
	response, err  := downloadClient.DownloadFile(req.Context(), reqInput)
	if err != nil {
		ResponseError(res, http.StatusBadRequest, err)
		return
	}
	Response(res, http.StatusOK, DownloadResponse{URL: response.Url, UniqID: response.UniqId})
}

func (d *DownloadController) decodeRequest(req *http.Request) (DownloadRequest, error) {
	reqContent := DownloadRequest{}
	if err := json.NewDecoder(req.Body).Decode(&reqContent); err != nil {
		return DownloadRequest{}, err
	}
	return reqContent, nil
}
