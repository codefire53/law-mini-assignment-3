package services
import (
	"bytes"
	"fmt"
	"log"
	"strconv"
	"time"
	"os"
	"io"
	"net"
	"net/http"	
	"golang.org/x/net/context"
	"path"
	"github.com/google/uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"github.com/streadway/amqp"
)
var ch *amqp.Channel
func init() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("%s",err)
	}
	ch, err = conn.Channel()
	if err != nil {
		log.Fatalf("%s",err)
	}
	err = ch.ExchangeDeclare(
		"1806141284",   // name
		"direct", // type
		false,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)

}

type Server struct{}

func StartServer() {
	listener, err := net.Listen("tcp", ":7001")
	if err != nil {
		panic(err)
	}
	srv := grpc.NewServer()
	RegisterDownloadServiceServer(srv, &Server{})
	reflection.Register(srv)
	if err = srv.Serve(listener); err != nil {
		panic(err)
	}

}
//DownloadFile function implementation
func (s *Server) DownloadFile(context context.Context, input *DownloadRequest) (*DownloadResponse, error) {
	id := uuid.New()
	idString := id.String()
	fileName := idString+"-"+path.Base(input.Url)
	go s.downloadFromUrl(input.Url, "./static/", idString)
	return &DownloadResponse{Url: "http://localhost:8080/static/"+fileName, UniqId:idString}, nil
}


func printDownloadPercent(done chan int64, path string, total int64, routingKey string, fileName string) {

	var stop bool = false

	for {
		select {
		case <-done:
			ch.Publish(
				"1806141284", // exchange
				routingKey,     // routing key
				false,  // mandatory
				false,  // immediate
				amqp.Publishing{
						ContentType: "text/plain",
						Body: []byte("http://localhost:8080/static/"+fileName),
			})
			fmt.Println("Done!")
			stop = true
		default:

			file, err := os.Open(path)
			if err != nil {
				log.Fatal(err)
			}

			fi, err := file.Stat()
			if err != nil {
				log.Fatal(err)
			}

			size := fi.Size()

			if size == 0 {
				size = 1
			}

			var percent float64 = float64(size) / float64(total) * 100
			ch.Publish(
				"1806141284", // exchange
				routingKey,     // routing key
				false,  // mandatory
				false,  // immediate
				amqp.Publishing{
						ContentType: "text/plain",
						Body:        []byte(fmt.Sprintf("%.0f", percent)),
				})
			fmt.Printf("%.0f", percent)
			fmt.Println("%")
		}

		if stop {
			break
		}

		time.Sleep(time.Second)
	}
}

func (s *Server) downloadFromUrl(url string, dest string, uniqueID string) {

	file := path.Base(url)

	log.Printf("Downloading file %s from %s\n", file, url)

	var path bytes.Buffer
	path.WriteString(dest)
	path.WriteString("/")
	path.WriteString(uniqueID)
	path.WriteString("-")
	path.WriteString(file)

	start := time.Now()

	out, err := os.Create(path.String())

	if err != nil {
		fmt.Println(path.String())
		panic(err)
	}

	defer out.Close()

	headResp, err := http.Head(url)

	if err != nil {
		panic(err)
	}

	defer headResp.Body.Close()

	size, err := strconv.Atoi(headResp.Header.Get("Content-Length"))

	if err != nil {
		panic(err)
	}

	done := make(chan int64)

	go printDownloadPercent(done, path.String(), int64(size), uniqueID, uniqueID+"-"+file)

	resp, err := http.Get(url)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	n, err := io.Copy(out, resp.Body)

	if err != nil {
		panic(err)
	}

	done <- n

	elapsed := time.Since(start)
	log.Printf("Download completed in %s", elapsed)
}
